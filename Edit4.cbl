       IDENTIFICATION DIVISION. 
       PROGRAM-ID. EDIT-4.
       AUTHOR. WARAPORN.

       DATA DIVISION. 
       WORKING-STORAGE SECTION. 
       01 Stars PIC *****.
       01  NumeOfStars PIC 9.

       PROCEDURE DIVISION .
       Begin.
           PERFORM VARYING NumeOfStars FROM 0 BY 1 UNTIL NumeOfStars > 5
              COMPUTE Stars = 10 ** ( 4 - NumeOfStars)
              INSPECT Stars CONVERTING "10" TO SPACES 
              DISPLAY NumeOfStars " = " Stars
           END-PERFORM
           STOP RUN.