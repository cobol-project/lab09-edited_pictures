       IDENTIFICATION DIVISION. 
       PROGRAM-ID. EDIT-5.
       AUTHOR. WARAPORN.

       DATA DIVISION. 
       WORKING-STORAGE SECTION. 
       01  SmallScaleNumber PIC VP(5)999 VALUE .00000423.
       01  LargeScaleNumber PIC 999P(5)V VALUE 45600000.00.
       01  ScaleBillions PIC 999P(9) VALUE ZEROS .

       01  SmallNumber PIC 9V9(8) VALUE 1.11111111.
       01  LargeNumber PIC 9(8)V9 VALUE 11111111.

       01  PrnSmall PIC 99.9(8) .
       01  PrnLarge PIC ZZ,ZZZ,ZZ9.99 .
       01  PrnBillions PIC ZZZ,ZZZ,ZZZ,ZZ9 .

       PROCEDURE DIVISION .
       Begin. 
           MOVE SmallScaleNumber TO PrnSmall 
           MOVE LargeScaleNumber TO PrnLarge 
           DISPLAY "Small scaled = " PrnSmall 
           DISPLAY "Large scaled = " PrnLarge 

           ADD SmallScaleNumber TO SmallNumber 
           ADD LargeScaleNumber TO LargeNumber 
           MOVE SmallNumber TO PrnSmall
           MOVE LargeNumber TO PrnLarge 
           DISPLAY "Small = " PrnSmall 
           DISPLAY "Large = " PrnLarge

           MOVE 123456789012 TO ScaleBillions 
           MOVE ScaleBillions TO PrnBillions 
           DISPLAY "Billions = " PrnBillions 
           STOP RUN. 